// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Vector2d.hpp"

#include <cmath>

#define EPSILON 1e-6

Vector2d::Vector2d() :
_x(0), _y(0)  {
}

Vector2d::Vector2d(double x, double y) :
		_x(x), _y(y) {
}

Vector2d Vector2d::operator *(double k) const {
	return Vector2d(k*_x, k*_y);
}

Vector2d Vector2d::operator +(const Vector2d & v) const {
	return Vector2d(_x+v._x, _y+v._y);
}

Vector2d Vector2d::operator -(const Vector2d & v) const {
	return Vector2d(_x-v._x, _y-v._y);
}

double dotProduct(const Vector2d & u, const Vector2d & v) {
	return u._x*v._x + u._y*v._y;
}

Vector2d & Vector2d::operator +=(const Vector2d & v) {
	_x += v._x;
	_y += v._y;
	return *this;
}

Vector2d & Vector2d::operator*=(double k) {
	_x *= k;
	_y *= k;
	return *this;
}

Vector2d::Vector2d(const Vector2d & A, const Vector2d & B) :
		_x(B._x-A._x), _y(B._y-A._y) {
}

void Vector2d::normalize() {
	double norme = sqrt(_x*_x + _y*_y);
	if (fabs(norme) > EPSILON) {
		_x /= norme;
		_y /= norme;
	}
}

Vector2d & Vector2d::operator +=(double k) {
	_x += k;
	_y += k;
	return *this;
}

double Vector2d::squaredNorm() const {
	return _x*_x + _y*_y;
}

Vector2d reflect(const Vector2d & incident, const Vector2d & normal) {
	return incident - normal * (2*dotProduct(incident, normal));
}





