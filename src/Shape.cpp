// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Shape.hpp"

#include <cmath>

Shape::Shape() {
}

Shape::~Shape() {
}

void ShapeSphere::draw(Cairo::RefPtr<Cairo::Context> cr) const {
	cr->save();
	cr->set_source_rgb(1.0, 0.0, 0.0);
	cr->set_line_width(2.0);
	cr->translate(_center._x, _center._y);
	cr->scale(_radius*0.5, _radius*0.5);
	cr->arc(0.0, 0.0, 1.0, 0.0, 2 * M_PI);
	cr->stroke();
	cr->restore();
}

void ShapeLine::draw(Cairo::RefPtr<Cairo::Context> cr) const {
	cr->save();
	cr->set_source_rgb(0.0, 0.0, 1.0);
	cr->set_line_width(2.0);
	cr->move_to(_p0._x, _p0._y);
	cr->line_to(_p1._x, _p1._y);
	cr->stroke();
	cr->restore();
}

ShapeSphere::ShapeSphere(int rayon, double x, double y) :
		_radius(rayon), _center(x, y) {
}

ShapeLine::ShapeLine(int x0, int y0, int x1, int y1) :
		_p0(x0, y0), _p1(x1, y1), _normal(y0-y1 , x1-x0) {
	_normal.normalize();
}

const Shape* ShapeSphere::collide(const ShapeSphere & ball, const Vector2d & ballVelocity, Vector2d & newVelocity) const {
	// compute the distance between the two spheres
	Vector2d sphereBallVec(_center, ball._center);
	double centerDistance = sphereBallVec.squaredNorm();
	double radiusSum = _radius + ball._radius;

	// collision if distance between centers is less than the sum of radius
	// fixme: not robust in case of high speed
	if (centerDistance < radiusSum*radiusSum) {
		// compute ball bounce
		Vector2d normal = sphereBallVec;
		normal.normalize();
		newVelocity = reflect(ballVelocity, normal);
		return this;
	}
	return 0;
}

const Shape* ShapeLine::collide(const ShapeSphere & ball, const Vector2d & ballVelocity, Vector2d & newVelocity) const {
	// compute distance from ball to wall
	Vector2d p0Ball(_p0, ball.getCenter());
	double ballWallDistance = dotProduct(p0Ball, _normal);
	// compute the position of the ball from the wall
	Vector2d p0p1(_p0, _p1);
	double ballWallPosition = dotProduct(p0p1, p0Ball);

	// collision if ball near the wall
	// fixme: not robust in case of high speed
	if (fabs(ballWallDistance) < ball.getRadius()
			and ballWallPosition > 0
			and ballWallPosition < p0p1.squaredNorm() ) {
		// compute ball bounce
		newVelocity = reflect(ballVelocity, _normal);
		return this;
	}
	return 0;
}
