// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Player.hpp"

#include <iostream>

Player::Player(int yMin, int yMax, double vitesse) :
_yMin(yMin), _yMax(yMax), _velocity(vitesse) {
}

void Player::startUpward() {
	_chronoUpward.start();
}

void Player::stopUpward() {
	_chronoUpward.stop();
}

void Player::startDownward() {
	_chronoDownward.start();
}

void Player::stopDownward() {
	_chronoDownward.stop();
}

void Player::update(Vector2d & position) {
	// update position according to player action (upward, downward)
	if (_chronoUpward.isRunning()) position._y -= _chronoUpward.delta()*_velocity;
	if (_chronoDownward.isRunning()) position._y += _chronoDownward.delta()*_velocity;

	// check position in [yMin, yMax]
	if (position._y < _yMin) position._y = _yMin;
	if (position._y > _yMax) position._y = _yMax;
}

