// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "ViewDrawing.hpp"

ViewDrawing::ViewDrawing(const Game & jeu) : _game(jeu) {
}

bool ViewDrawing::on_expose_event(GdkEventExpose *event)
{
	// get window
	Glib::RefPtr<Gdk::Window> window = get_window();

	if(window) {
		// get drawing context
		Cairo::RefPtr<Cairo::Context> cr = window->create_cairo_context();

		// clean area
		cr->rectangle(event->area.x, event->area.y, event->area.width, event->area.height);
		cr->clip();

		// draw the ball
		_game.getBall().draw(cr);

		// draw the others shapes (walls, paddles)
		for (const Shape* ptrObject : _game.getShapes())
			ptrObject->draw(cr);
	}

	return true;
}

void ViewDrawing::update() {
	// get window
	Glib::RefPtr<Gdk::Window> win = get_window();
	if (win) {
		// invalidate window (call on_expose_event)
		win->invalidate(false);
	}
}
