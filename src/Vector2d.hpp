// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef VECTOR2D_HPP_
#define VECTOR2D_HPP_

/// \brief 2D vector (double precision)
///
/// See also \link dotProduct \endlink and  \link reflect \endlink .
///
struct Vector2d {
	/// \brief x coordinate
	///
	double _x;

	/// \brief y coordinate
	///
	double _y;

	/// \brief construct a nul vector
	///
	Vector2d();

	/// \brief construct and define a vector
	///
	Vector2d(double x, double y);

	/// \brief construct a vector between to "points"
	///
	Vector2d(const Vector2d & A, const Vector2d & B);

	/// \brief vector-scalar product
	///
	Vector2d operator*(double k) const;

	/// \brief vector-vector addition
	///
	Vector2d operator+(const Vector2d & v) const;

	/// \brief vector-vector soustraction
	///
	Vector2d operator-(const Vector2d & v) const;

	/// \brief add a vector
	///
	Vector2d & operator+=(const Vector2d & v);

	/// \brief add a scalar
	///
	Vector2d & operator+=(double k);

	/// \brief multiply by a scalar
	///
	Vector2d & operator*=(double k);

	/// \brief normalize
	///
	void normalize();

	/// \brief compute squared norm
	///
	double squaredNorm() const;
};

/// \file Vector2d.hpp
///

/// \brief compute dot product
///
double dotProduct(const Vector2d & u, const Vector2d & v);

/// \brief compute reflected vector
///
Vector2d reflect(const Vector2d & incident, const Vector2d & normal);

#endif
