// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef SHAPE_HPP_
#define SHAPE_HPP_

#include "Vector2d.hpp"

#include <gtkmm.h>

class ShapeSphere;

/// \brief abstract class for displayable shapes
///
class Shape {
public:
	Shape();
	virtual ~Shape();

	/// \brief abstract drawing method
	///
	virtual void draw(Cairo::RefPtr<Cairo::Context> cr) const = 0 ;

	/// \brief abstract collision detection method
	///
	/// If collision, compute bounce
	///
	virtual const Shape* collide(const ShapeSphere & ball, const Vector2d & ballVelocity, Vector2d & newVelocity) const = 0;
};

/// \brief sphere shape (for ball and paddles)
///
class ShapeSphere : public Shape {
	int _radius;
	Vector2d _center;
public:

	/// \brief use this constructor (set the radius and the center of the sphere)
	///
	ShapeSphere(int radius, double x, double y);

	/// \brief draw sphere
	///
	void draw(Cairo::RefPtr<Cairo::Context> cr) const;

	/// \brief compute ball/sphere collision
	///
	virtual const Shape* collide(const ShapeSphere & ball, const Vector2d & ballVelocity, Vector2d & newVelocity) const;

	/// \brief the center of the sphere
	///
	Vector2d & getCenter() {return _center;}

	/// \brief the center of the sphere
	///
	const Vector2d & getCenter() const {return _center;}

	/// \brief radius
	///
	const int & getRadius() const {return _radius; }
};

/// \brief line shape (for walls)
///
class ShapeLine : public Shape {
	Vector2d _p0;
	Vector2d _p1;
	Vector2d _normal;

public:
	/// \brief define the two points of the line
	///
	ShapeLine(int x0, int y0, int x1, int y1);

	/// \brief draw line
	///
	void draw(Cairo::RefPtr<Cairo::Context> cr) const;

	/// \brief compute ball/line collision
	///
	virtual const Shape* collide(const ShapeSphere & ball, const Vector2d & ballVelocity, Vector2d & newVelocity) const;
};

#endif
