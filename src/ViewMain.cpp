// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "ViewMain.hpp"

ViewMain::ViewMain(int argc, char **argv):
_kit(argc, argv) {
}

void ViewMain::run()
{
	_kit.run(_viewWindow);
}
