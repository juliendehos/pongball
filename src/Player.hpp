// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include "Chrono.hpp"
#include "Vector2d.hpp"

/// \brief control a player
///
class Player {
	int _yMin;
	int _yMax;
	double _velocity;

	Chrono _chronoUpward;
	Chrono _chronoDownward;

public:
	/// \brief use this constructor
	///
	Player(int yMin, int yMax, double velocity);

	/// \brief start moving upward
	///
	void startUpward();

	/// \brief end moving upward
	///
	void stopUpward();

	/// \brief start moving downward
	///
	void startDownward();

	/// \brief end moving downward
	///
	void stopDownward();

	/// \brief update player position
	///
	/// this method must be called each second at least (GTK)
	///
	/// \param position of the player, to update
	///
	void update(Vector2d & position);
};

#endif
