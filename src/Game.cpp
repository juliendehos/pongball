// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Game.hpp"

Game::Game(int width, int height) : _width(width), _height(height),
_ball(10, 0, 0), _leftPlayer(30, height-30, 500), _rightPlayer(30, height-30, 500) {

	// configure the game elements
	_leftPaddle = new ShapeSphere(20, 0, 0);
	_shapes.push_back(_leftPaddle);

	_rightPaddle = new ShapeSphere(20, 0, 0);
	_shapes.push_back(_rightPaddle);

	_shapes.push_back(new ShapeLine(10, 10, width-10, 10));
	_shapes.push_back(new ShapeLine(10, height-10, width-10, height-10));
	_shapes.push_back(new ShapeLine(width*0.5, height*0.5-50, width*0.5, height*0.5+50));

	// initialize RNG
	srand(time(0));

	// reset game
	reset();
}

Game::~Game() {
	// remove game elements
	for (std::vector<Shape*>::iterator it = _shapes.begin(); it != _shapes.end(); ++it) {
		delete *it;
	}
}

void Game::update() {
	_leftPlayer.update(_leftPaddle->getCenter());
	_rightPlayer.update(_rightPaddle->getCenter());

	double deltaT = _chrono.delta();
	// check collisions
	for (const Shape* ptrShape : _shapes) {
		Vector2d nouvelleVitesse;
		const Shape * collision =  ptrShape->collide(_ball, _ballVelocity, nouvelleVitesse);
		if (collision and collision != _lastCollision) {
			_lastCollision = collision;
			_ballVelocity = nouvelleVitesse;
			if (_ballVelocity.squaredNorm() < 1e6)
				_ballVelocity *= 1.1;
		}
	}

	// update ball position
	_ball.getCenter() += _ballVelocity * deltaT;
}

void Game::reset() {
	// reset ball and paddles positions
	_ball.getCenter() = Vector2d(_width*0.5, 100);
	_leftPaddle->getCenter() = Vector2d(20, _height*0.5);
	_rightPaddle->getCenter() = Vector2d(_width-20, _height*0.5);

	// set initial velocity of the ball to a random vector
	int vy = (rand()/(double)RAND_MAX)*400 - 200;	// vy in [-200, 200]
	int vx = (rand()/(double)RAND_MAX)*100 + 100;	// vx in [100, 200]
	double dx = (rand()/(double)RAND_MAX) - 0.5;
	if (dx < 0) vx = -vx;							// vx in [100, 200] or [-100, -200]
	_ballVelocity = Vector2d(vx, vy);

	_lastCollision = 0;
	_chrono.start();
}

bool Game::hasWon(std::string & message)
{
	int xBalle = _ball.getCenter()._x;

	if (xBalle < 10) {
		message = std::string("Winner: right player");
		return true;
	}

	if (xBalle > _width - 10) {
		message = std::string("Winner: left player");
		return true;
	}

	return false;
}
