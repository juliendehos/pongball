// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef CHRONO_HPP_
#define CHRONO_HPP_

#include <ctime>

/// \brief measure time duration.
///
/// possible usages :
/// - start, stop, elapsed : measure a duration
/// - start, elapsedRunning, ..., elapsedRunning, stop : measure durations from one starting time
/// - start, delta, ..., delta, stop : measure successive durations (reset starting time)
///
class Chrono {
	clock_t _t0;
	clock_t _t1;
	bool _running;

public:
	/// \brief Use this constructor.
	///
	Chrono() : _running(false) {}

	/// \brief Start measure.
	///
	void start() {
		_running = true;
		_t0 = clock();
	}

	/// \brief Stop measure.
	///
	void stop() {
		_t1 = clock();
		_running = false;
	}

	/// \brief Return elapsed time between last start/delta and following stop.
	///
	double elapsed() {
		return (_t1 - _t0)/(double)CLOCKS_PER_SEC;
	}

	/// \brief Return elapsed time since last start/delta.
	///
	double elapsedRunning() {
		_t1 = clock();
		return (_t1 - _t0)/(double)CLOCKS_PER_SEC;
	}

	/// \brief Return elapsed time since last start/delta and restart measure.
	///
	double delta() {
		_t1 = clock();
		double delta = (_t1 - _t0)/(double)CLOCKS_PER_SEC;
		_t0 = _t1;
		return delta;
	}

	/// \brief is measure running.
	///
	bool isRunning() {
		return _running;
	}
};

#endif
