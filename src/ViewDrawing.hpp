// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef VIEWDRAWING_HPP_
#define VIEWDRAWING_HPP_

#include "Game.hpp"

class ViewWindow;

#include <gtkmm/drawingarea.h>

/// \brief drawing area for PongBall
///
class ViewDrawing : public Gtk::DrawingArea {
	const Game & _game;

public:
	/// \brief constructor
	///
	ViewDrawing(const Game & game);

	/// \brief update display
	///
	void update();

protected:
	/// \brief callback function
	///
	virtual bool on_expose_event(GdkEventExpose* event);
};

#endif
