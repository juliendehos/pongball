// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef GAME_HPP_
#define GAME_HPP_

#include "Player.hpp"
#include "Shape.hpp"
#include "Vector2d.hpp"

/// \brief Implement a PongBall game.
///
/// Run the game (ball, player, collision, end of game...)
///
class Game {
	const Shape* _lastCollision;
	Chrono _chrono;

	int _width;
	int _height;

	std::vector<Shape*> _shapes;

	ShapeSphere _ball;
	Vector2d _ballVelocity;

	Player _leftPlayer;
	ShapeSphere * _leftPaddle;

	Player _rightPlayer;
	ShapeSphere * _rightPaddle;

public:
    /// \brief constructor; set the size of the playing area.
    ///
	Game(int width, int height);

	virtual ~Game();

    /// \brief reset game
    ///
    /// reset players and ball position; set the initial velocity of the ball to a random vector
    ///
	void reset();

    /// \brief update players and ball
    ///
	void update();

    /// \brief check if someone has won the game
    //
    // \param message (set if someone has won)
    ///
	bool hasWon(std::string & message);

    /// \brief ball shape
    ///
    const ShapeSphere & getBall() const { return _ball; }

    /// \brief right player
    ///
    Player & getRightPlayer() { return _rightPlayer; }

    /// \brief left player
    ///
    Player & getLeftPlayer() { return _leftPlayer; }

    /// \brief returns displayable shapes (ball, paddles, walls)
    ///
    const std::vector<Shape*> & getShapes() const { return _shapes; }
};

#endif
