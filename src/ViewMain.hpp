// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef VIEWMAIN_HPP_
#define VIEWMAIN_HPP_

#include "ViewWindow.hpp"

/// \brief main class for PongBall; use this to create program
///
class ViewMain {
	Gtk::Main _kit;
	ViewWindow _viewWindow;

public:
	/// \brief constructs program
	///
	ViewMain(int argc, char ** argv);

	/// \brief run program
	///
	void run();
};

#endif
