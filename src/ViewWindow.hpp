// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef VIEWWINDOW_HPP_
#define VIEWWINDOW_HPP_

#include "ViewDrawing.hpp"
#include "Game.hpp"

#include <gtkmm.h>
#include <vector>

/// \brief Main window for PongBall.
///
class ViewWindow : public Gtk::Window {
	Game _game;
	ViewDrawing _viewDrawing;
	sigc::connection _idleConnection;

public:
	/// \brief use this constructor
	///
	ViewWindow();

	/// \brief run main loop
	///
	void run();

	/// \brief start a new game
	///
	void startPlaying();

	/// \brief stop game
	///
	void stopPlaying();

	/// \brief display end message and ask for new game
	///
	bool displayMessage(Glib::ustring & titre, Glib::ustring & texte);

protected:
	/// \brief idle callback (update game)
	///
	bool handle_idle();

	/// \brief key press callback (paddles)
	///
	bool on_key_press_event(GdkEventKey * event);

	/// \brief key release callback (paddles)
	///
	bool on_key_release_event(GdkEventKey * event);
};

#endif
