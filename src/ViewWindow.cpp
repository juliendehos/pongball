// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "ViewWindow.hpp"

const int WIDTH = 800;
const int HEIGHT = 600;

ViewWindow::ViewWindow() :
	 _game(WIDTH, HEIGHT), _viewDrawing(_game) {
	// set window parameters
	set_title("PongBall");
	set_size_request(WIDTH, HEIGHT);
	set_resizable(false);
	add(_viewDrawing);
	show_all();

	// display welcome message
	Glib::ustring titreAccueil("PongBall");
	Glib::ustring texteAccueil("Left player: e/d keys \nRight player: p/l keys");
	bool veutJouer = displayMessage(titreAccueil, texteAccueil);

	if (not veutJouer) exit(0);
	else startPlaying();
}

void ViewWindow::startPlaying() {
	_game.reset();
	if (_idleConnection.connected()) _idleConnection.disconnect();
	_idleConnection = Glib::signal_idle().connect(sigc::mem_fun(*this, &ViewWindow::handle_idle));
}

void ViewWindow::stopPlaying() {
	_idleConnection.disconnect();
	_game.getLeftPlayer().stopUpward();
	_game.getLeftPlayer().stopDownward();
	_game.getRightPlayer().stopUpward();
	_game.getRightPlayer().stopDownward();
}

bool ViewWindow::displayMessage(Glib::ustring & titre, Glib::ustring & texte) {
	Gtk::MessageDialog dialogue(*this, texte + "\n\nNew game ?", false,
			Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO, true);
	dialogue.set_title(titre);
	dialogue.set_default_response(Gtk::RESPONSE_YES);
	int reponse = dialogue.run();
	return reponse == Gtk::RESPONSE_YES;
}

bool ViewWindow::handle_idle() {
	std::string message;

	// someone has won
	if (_game.hasWon(message)) {
		// stop game and display message
		stopPlaying();
		Glib::ustring titre("End of game");
		Glib::ustring texte(message);
		bool nouveauJeu = displayMessage(titre, texte);

		// start a new game or quit
		if (nouveauJeu) {
			startPlaying();
		}
		else {
			Gtk::Main::quit();
		}
	}
	else { // no one has won
		// update game and gui
		_game.update();
		_viewDrawing.update();
	}
	return true;
}

bool ViewWindow::on_key_press_event(GdkEventKey *event) {
	switch (event->keyval) {
	case 'e' : _game.getLeftPlayer().startUpward(); break;
	case 'd' : _game.getLeftPlayer().startDownward(); break;
	case 'p' : _game.getRightPlayer().startUpward(); break;
	case 'l' : _game.getRightPlayer().startDownward(); break;
	}
	return true;
}

bool ViewWindow::on_key_release_event(GdkEventKey *event) {
	switch (event->keyval) {
	case 'e' : _game.getLeftPlayer().stopUpward(); break;
	case 'd' : _game.getLeftPlayer().stopDownward(); break;
	case 'p' : _game.getRightPlayer().stopUpward(); break;
	case 'l' : _game.getRightPlayer().stopDownward(); break;
	}
	return true;
}




