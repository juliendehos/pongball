// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

/// \mainpage PongBall
///
/// Pong with circular paddles
///
/// Description :
/// - graphical interface
/// - 2D scene with walls, paddles and a ball
/// - control paddles with keyboard
/// - ball/wall and ball/paddle collision
/// - animate the ball (increase speed at each bounch)
/// - detect end of game
///
/// \bug collision detection is not robust (when the ball has high velocity)
/// \fixme refactor the code
///

#include "ViewMain.hpp"

int main (int argc, char ** argv) {
	ViewMain viewMain(argc, argv);
	viewMain.run();
	return 0;
}
