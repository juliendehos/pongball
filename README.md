# PongBall 
[![Build status](https://gitlab.com/juliendehos/pongball/badges/master/build.svg)](https://gitlab.com/juliendehos/pongball/pipelines) 

Pong with circular paddles

![](image_pongball.jpg)

## Build & run
install gtkmm-2.4
```
make  
./bin/PongBall.out
```

